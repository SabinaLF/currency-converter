import React, { useEffect } from "react";
import { useDispatch, useSelector } from 'react-redux';
import uuid from 'uuid/v4';
import "./style/index.scss";
import spinner from "./spinner.svg";
import Dashboard from './components/dashboard';

const App = () => {

  const dispatch = useDispatch();
  const { items, error, isFetched } = useSelector(state => ({
    items: state.fetch.items,
    error: state.fetch.error,
    isFetched: state.fetch.isFetched
  }));
  const apiKey = process.env.REACT_APP_API_KEY;
  const url = `http://www.apilayer.net/api/live?access_key=${apiKey}&format=1`;

  useEffect(() => {
    const fetchData = () => {
      dispatch({ type: 'FETCH_DATA_PENDING' });
      fetch(url, { method: 'GET' })
        .then(resp => resp.json())
        .then(resp => {
          const fetchedItems = Object.keys(resp.quotes).map(key => {
            return { id: uuid(), currencyName: key.slice(3), currencyValue: resp.quotes[key] }
          });
          const keys = fetchedItems.map(item => item.currencyName)
          dispatch({ type: 'SET_KEYS', keys });

          const eur = fetchedItems.find(i => i.currencyName === 'EUR')
          const gbp = fetchedItems.find(i => i.currencyName === 'GBP')

          dispatch({
            type: 'SET_CONVERT', convert: {
              from: eur.currencyName,
              to: gbp.currencyName,
              amountFrom: 1,
              amountTo: 0.85,
              rateFrom: eur.currencyValue,
              rateTo: gbp.currencyValue
            }
          })
          dispatch({ type: 'FETCH_DATA_SUCCESS', items: fetchedItems });
        })
        .catch(error => dispatch({ type: 'FETCH_DATA_FAILURE', error }))
    }
    fetchData()
  }, [dispatch, url]);

  return (
    <>
      {error ? <div className='error'>{error}</div> : null}
      {!isFetched && !error && <img src={spinner} className="spinner" alt="Loading..." />}
      {(isFetched && items.length) ? <Dashboard /> : null}
    </>
  );
}

export default App;
