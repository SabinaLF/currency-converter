import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { Provider } from "react-redux";
import { createStore } from 'redux';
import reducers from './reducers/reducers';
import { composeWithDevTools } from 'redux-devtools-extension';
import * as serviceWorker from './serviceWorker';

const initialState = {
  fetch: {
    items: [],
    isFetched: false,
    error: null,
  },
  convert: {
    from: 'EUR',
    to: 'GBP',
    amountFrom: 1,
    amountTo: 1,
    rateFrom: 3.673204,
    rateTo: 3.673204
  },
  keys: []
};

const store = createStore(reducers, initialState, composeWithDevTools());

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>
  , document.getElementById('root'));
serviceWorker.unregister();
