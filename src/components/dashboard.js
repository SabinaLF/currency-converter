import React from "react";
import { useDispatch, useSelector } from 'react-redux';
import Select from './select';

const Dashboard = () => {
    const dispatch = useDispatch();
    const { items, from, to, amountFrom, amountTo, rateFrom, rateTo } = useSelector(state => ({
        items: state.fetch.items,
        from: state.convert.from,
        to: state.convert.to,
        amountFrom: state.convert.amountFrom,
        amountTo: state.convert.amountTo,
        rateFrom: state.convert.rateFrom,
        rateTo: state.convert.rateTo
    }));

    const calculateAmount = (type, af, at, rt, rf) => type === 'from' ? { _amountTo: +af, _amountFrom: +(at * (rf / rt)) } : { _amountFrom: +at, _amountTo: +(af * (rt / rf)) }
    const calculateSelectAmount = (type, af, at, rt, rf) => type === 'from' ? { _amountTo: +(af * (rt / rf)), _amountFrom: +af } : { _amountFrom: +(at * (rf / rt)), _amountTo: +at }

    const findItem = (items, value) => {
        const item = items.find(i => i.currencyName === value)
        const rate = item.currencyValue
        return rate
    }

    const onSelectFromChange = e => {
        const rate = findItem(items, e.target.value);
        const { _amountTo } = calculateSelectAmount('from', amountFrom, amountTo, rate, rateTo)
        dispatch({ type: 'GET_AMOUNT_FROM', from: e.target.value, amountFrom, amountTo: _amountTo, rateFrom: rate })
    }

    const onSelectToChange = e => {
        const rate = findItem(items, e.target.value);
        const { _amountFrom } = calculateSelectAmount('to', amountFrom, amountTo, rate, rateFrom)
        dispatch({ type: 'GET_AMOUNT_TO', to: e.target.value, amountFrom: _amountFrom, amountTo, rateTo: rate })
    }

    const onInputChange = e => {
        if (e.target.name === 'inputFrom') {
            const { _amountFrom, _amountTo } = calculateAmount('from', e.target.value, amountTo, rateFrom, rateTo)
            dispatch({ type: 'SET_AMOUNT_TO', from, amountFrom: _amountFrom, amountTo: _amountTo })
        } else if (e.target.name === 'inputTo') {
            const { _amountFrom, _amountTo } = calculateAmount('to', amountFrom, e.target.value, rateFrom, rateTo)
            dispatch({ type: 'SET_AMOUNT_FROM', to, amountFrom: _amountFrom, amountTo: _amountTo })
        }
    }

    return (
        <div className="Dashboard">
            <h1 className="title">Currency Converter</h1>
            <div className="form">
                <div className="from">
                    <p className="rate">1 {from} = {rateFrom}</p>
                    <Select type="from" onChange={onSelectFromChange}/>
                    <input className="Input"
                        type='number'
                        value={amountFrom}
                        name="inputFrom"
                        onChange={onInputChange}
                    />
                </div>
                <div className="swap"><i className="material-icons md-dark" onClick={() => { 
                    dispatch({ type: 'SWITCH_BETWEEN', to, from, rateTo, rateFrom, amountTo, amountFrom }) }
                    }>swap_horiz</i></div>
                <div className="to">
                    <p className="rate">1 {to} = {rateTo}</p>
                    <Select type="to" onChange={onSelectToChange}/>
                    <input className="Input"
                        type='number'
                        value={amountTo}
                        name="inputTo"
                        onChange={onInputChange}
                    />
                </div>
            </div>
        </div>
    );
}

export default Dashboard;