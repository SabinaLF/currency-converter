import React from "react";
import { useSelector } from 'react-redux';

const Select = ({ type, onChange }) => {
  const convert = useSelector(state => (state.convert))
  const keys = useSelector(state => (state.keys));

  let val

  if (type === 'from') {
    val = convert.from
  } else {
    val = convert.to
  }

  return (
    <select className="Select" value={val} onChange={onChange}>{keys.map(k => (
      <option key={k} value={k}>
        {k}
      </option>))}
    </select>
  );
}

export default Select;