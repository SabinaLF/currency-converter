const fetchingDataReducer = (state = {}, action) => {
  switch (action.type) {
    case 'FETCH_DATA_PENDING':
      return {
        ...state,
        isFetched:false,
        error: null
      }
    case 'FETCH_DATA_SUCCESS':
      return {
        ...state,
        isFetched:true,
        items: action.items
      };
    case 'FETCH_DATA_FAILURE':
      return {
        ...state,
        error: action.error
      };
    default:
      return state;
  }
}
export default fetchingDataReducer;
