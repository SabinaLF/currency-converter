import { combineReducers } from 'redux';
import fetchingDataReducer from '../reducers/fetchingDataReducer';
import convertReducer from '../reducers/convertReducer';
import keysReducer from '../reducers/keysReducer';


const reducers = combineReducers({
    fetch: fetchingDataReducer,
    convert: convertReducer,
    keys: keysReducer
});
export default reducers;
