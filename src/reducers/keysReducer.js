const keysReducer = (state = {}, action) => {
    switch (action.type) {
        case 'SET_KEYS':
            return action.keys
        default:
            return state;
    }
}
export default keysReducer;
