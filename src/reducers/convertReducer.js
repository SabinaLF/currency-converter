const convertReducer = (state = {}, action) => {
  switch (action.type) {
    case 'SET_CONVERT':
      return action.convert
    case 'GET_AMOUNT_FROM':
      return {
        ...state,
        from: action.from,
        rateFrom: action.rateFrom,
        amountFrom: action.amountFrom,
        amountTo: action.amountTo
      }
    case 'GET_AMOUNT_TO':
      return {
        ...state,
        to: action.to,
        rateTo: action.rateTo,
        amountTo: action.amountTo,
        amountFrom: action.amountFrom
      }
    case 'SET_AMOUNT_FROM':
      return {
        ...state,
        amountFrom: action.amountTo,
        amountTo: action.amountFrom
      }
    case 'SET_AMOUNT_TO':
      return {
        ...state,
        amountTo: action.amountFrom,
        amountFrom: action.amountTo
      }
    case 'SWITCH_BETWEEN':
      return {
        ...state,
        to: action.from,
        from: action.to,
        rateTo: action.rateFrom,
        rateFrom: action.rateTo,
        amountTo: action.amountFrom,
        amountFrom: action.amountTo
      };
    default:
      return state;
  }
}
export default convertReducer;
